package com.example.testappex1.base

import android.app.Activity
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.da.phrasi.shared.helpers.ViewMatchersHelper
import org.junit.Rule
import org.junit.rules.RuleChain
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
abstract class BaseInstrumentedTest<T : Activity>(clazz: Class<T>) {

//    @Rule
//    @JvmField
    val testRule: ActivityTestRule<T> = ActivityTestRule(clazz)

    @JvmField
    @Rule
    val ruleChain: RuleChain = RuleChain
        .outerRule(testRule)

    val viewMatchersHelper: ViewMatchersHelper = ViewMatchersHelper()


}