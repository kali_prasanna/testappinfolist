package com.example.testappex1.base

import android.app.Activity
import android.support.test.InstrumentationRegistry.getInstrumentation
import android.support.test.espresso.IdlingResource
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import android.support.test.runner.lifecycle.Stage
import android.widget.ProgressBar
import com.example.testappex1.R

class ProgressIdlingResource(private val name: String) : IdlingResource {

    private var resourceCallback: IdlingResource.ResourceCallback? = null
    private var isIdle = false

    override fun getName(): String {
        return name
    }

    override fun isIdleNow(): Boolean {
        if (isIdle) return true

        val activity = getCurrentActivity()
        isIdle = activity.findViewById<ProgressBar>(R.id.swipe_container)?.isAnimating == false
        if (isIdle) {
            resourceCallback?.onTransitionToIdle()
        }
        return isIdle
    }

    override fun registerIdleTransitionCallback(resourceCallback: IdlingResource.ResourceCallback?) {
        this.resourceCallback = resourceCallback
    }

    private fun getCurrentActivity(): Activity {
        getInstrumentation().waitForIdleSync()
        lateinit var currentActivity: Activity
        val resumedActivities = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED)
        if (resumedActivities.iterator().hasNext()) {
            currentActivity = resumedActivities.iterator().next()
        }
        return currentActivity
    }

}