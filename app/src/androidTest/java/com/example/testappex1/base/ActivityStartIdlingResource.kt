package com.example.testappex1.base

import android.app.Activity
import android.support.test.espresso.IdlingResource
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import android.support.test.runner.lifecycle.Stage

class ActivityStartIdlingResource : IdlingResource {

    private var resourceCallback: IdlingResource.ResourceCallback? = null
    private var isIdle = false
    private var activityName = ""

    override fun getName(): String {
        return ActivityStartIdlingResource::class.java.name
    }

    override fun isIdleNow(): Boolean {
        if (activityName.isEmpty()) {
            return true
        }

        val activity = getCurrentActivity()
        activity?.let {
            isIdle = activity::class.java.name == activityName
        }
        if (isIdle) {
            resourceCallback?.onTransitionToIdle()
        }
        return isIdle
    }

    override fun registerIdleTransitionCallback(resourceCallback: IdlingResource.ResourceCallback?) {
        this.resourceCallback = resourceCallback
    }

    fun setActivityName(activityName: String = "") {
        this.activityName = activityName
    }

    fun getCurrentActivity(): Activity? {
        var currentActivity: Activity? = null
        val resumedActivities = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED)
        if (resumedActivities?.iterator()?.hasNext() == true) {
            currentActivity = resumedActivities.iterator().next()
        }
        return currentActivity
    }

}