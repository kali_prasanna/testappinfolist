package com.example.testappex1.helpers

import android.support.annotation.IdRes
import android.support.test.espresso.Espresso
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.BoundedMatcher
import android.support.test.espresso.matcher.ViewMatchers
import android.support.v7.widget.RecyclerView
import android.view.View
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.TypeSafeMatcher

class Utils {

    /**
     * Get Count for a RecyclerView
     * */
    fun getCountFromRecyclerView(@IdRes recyclerViewId: Int): Int {
        var count = 0
        val matcher = object : TypeSafeMatcher<View>() {
            override fun matchesSafely(item: View): Boolean {
                count = (item as RecyclerView).adapter?.itemCount ?: 0
                return true
            }

            override fun describeTo(description: Description) {}
        }
        Espresso.onView(Matchers.allOf(ViewMatchers.withId(recyclerViewId), ViewMatchers.isDisplayed()))
            .check(ViewAssertions.matches(matcher))
        val result = count
        count = 0
        return result
    }

    fun withViewAtPosition(position: Int, itemMatcher: Matcher<View>): Matcher<View> {
        val matcher = object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description?) {
                itemMatcher.describeTo(description);
            }

            override fun matchesSafely(item: RecyclerView?): Boolean {
                val viewHolder: RecyclerView.ViewHolder? = item?.findViewHolderForAdapterPosition(position);
                return viewHolder != null && itemMatcher.matches(viewHolder.itemView);
            }
        }
        return matcher
    }

}