package com.da.phrasi.shared.helpers

import android.support.annotation.IdRes
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers

class ViewMatchersHelper {

    fun isViewCompletelyDisplayed(@IdRes viewId: Int) {
        onView(ViewMatchers.withId(viewId)).check(matches(ViewMatchers.isCompletelyDisplayed()))
    }

}