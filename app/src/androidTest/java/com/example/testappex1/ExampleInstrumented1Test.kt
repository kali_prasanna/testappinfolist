package com.example.testappex1

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.IdlingPolicies
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.action.ViewActions.swipeDown
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import com.example.testappex1.base.ActivityStartIdlingResource
import com.example.testappex1.base.BaseInstrumentedTest
import com.example.testappex1.feature.infolist.InfoListActivity
import com.example.testappex1.feature.infolist.InfoListRecyclerViewAdapter
import com.example.testappex1.helpers.Utils
import org.hamcrest.CoreMatchers.allOf
import org.junit.After
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import java.util.concurrent.TimeUnit

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4::class)
class ExampleInstrumented1Test : BaseInstrumentedTest<InfoListActivity>(InfoListActivity::class.java) {
    private var activityStartIdlingResource = ActivityStartIdlingResource()
    lateinit var utils: Utils

    @Before
    fun before() {
        IdlingRegistry.getInstance().register(activityStartIdlingResource)
        IdlingPolicies.setMasterPolicyTimeout(5, TimeUnit.MINUTES)
        IdlingPolicies.setIdlingResourceTimeout(5, TimeUnit.MINUTES)
        Intents.init()
        utils = Utils()
    }

    @After
    fun after() {
        IdlingRegistry.getInstance().unregister(activityStartIdlingResource)
        Intents.release()
    }

    @Test
    fun test1CheckToolbarText() {
        activityStartIdlingResource.setActivityName(InfoListActivity::class.java.name)

        Thread.sleep(5000)
        viewMatchersHelper.isViewCompletelyDisplayed(R.id.tv_title)

        IdlingRegistry.getInstance().unregister(activityStartIdlingResource)
    }

    @Test
    fun test2ShowListItemWithPullToRefresh() {
        activityStartIdlingResource.setActivityName(InfoListActivity::class.java.name)

        var infoSize = utils.getCountFromRecyclerView(R.id.rv_infolist)
        for (i in 0 until infoSize) {
            onView(withId(R.id.rv_infolist)).check(
                ViewAssertions.matches(
                    utils.withViewAtPosition(
                        i, ViewMatchers.hasDescendant(
                            (allOf(withId(R.id.iv_item_info), isCompletelyDisplayed()))
                        )
                    )
                )
            )
            onView(withId(R.id.rv_infolist)).check(
                ViewAssertions.matches(
                    utils.withViewAtPosition(
                        i, ViewMatchers.hasDescendant(
                            (allOf(withId(R.id.tv_item_dec), isCompletelyDisplayed()))
                        )
                    )
                )
            )
            onView(withId(R.id.rv_infolist)).check(
                ViewAssertions.matches(
                    utils.withViewAtPosition(
                        i, ViewMatchers.hasDescendant(
                            (allOf(withId(R.id.tv_item_title), isCompletelyDisplayed()))
                        )
                    )
                )
            )
            if (i <= infoSize - 1)
                onView(withId(R.id.rv_infolist)).perform(
                    RecyclerViewActions.scrollToPosition<InfoListRecyclerViewAdapter.ItemrowListHolder>(i + 1)
                )
        }

        onView(withId(R.id.rv_infolist)).perform(
            RecyclerViewActions.scrollToPosition<InfoListRecyclerViewAdapter.ItemrowListHolder>(0)
        )
        onView(withId(R.id.swipe_container)).perform(swipeDown())
        Thread.sleep(2500)
        infoSize = utils.getCountFromRecyclerView(R.id.rv_infolist)
        for (i in 0 until infoSize) {
            onView(withId(R.id.rv_infolist)).check(
                ViewAssertions.matches(
                    utils.withViewAtPosition(
                        i, ViewMatchers.hasDescendant(
                            (allOf(withId(R.id.iv_item_info), isCompletelyDisplayed()))
                        )
                    )
                )
            )
            onView(withId(R.id.rv_infolist)).check(
                ViewAssertions.matches(
                    utils.withViewAtPosition(
                        i, ViewMatchers.hasDescendant(
                            (allOf(withId(R.id.tv_item_dec), isCompletelyDisplayed()))
                        )
                    )
                )
            )
            onView(withId(R.id.rv_infolist)).check(
                ViewAssertions.matches(
                    utils.withViewAtPosition(
                        i, ViewMatchers.hasDescendant(
                            (allOf(withId(R.id.tv_item_title), isCompletelyDisplayed()))
                        )
                    )
                )
            )
            if (i <= infoSize - 1)
                onView(withId(R.id.rv_infolist)).perform(
                    RecyclerViewActions.scrollToPosition<InfoListRecyclerViewAdapter.ItemrowListHolder>(i + 1)
                )
        }
        IdlingRegistry.getInstance().unregister(activityStartIdlingResource)
    }


}
