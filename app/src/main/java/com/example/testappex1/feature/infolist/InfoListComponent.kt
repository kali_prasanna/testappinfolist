package com.example.testappex1.feature.infolist

import com.example.testappex1.di.AppComponent
import com.example.testappex1.feature.shared.di.FeatureScope
import dagger.Component

@FeatureScope
@Component(dependencies = [AppComponent::class], modules = [InfoListModule::class])
interface InfoListComponent {
    fun inject(infoListActivity: InfoListActivity)
}