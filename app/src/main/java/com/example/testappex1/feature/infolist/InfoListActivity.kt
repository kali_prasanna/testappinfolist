package com.example.testappex1.feature.infolist

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.ActionBar
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.example.testappex1.R
import com.example.testappex1.databinding.ActivityInfoListBinding
import com.example.testappex1.di.AppComponent
import com.example.testappex1.feature.shared.view.DaggerActivity
import javax.inject.Inject

class InfoListActivity : DaggerActivity(), InfoListRecyclerViewAdapter.InfoListRecyclerViewListener, InfoListInteractor,
    SwipeRefreshLayout.OnRefreshListener {
    private lateinit var binding: ActivityInfoListBinding
    private var actionBar: ActionBar? = null
    @Inject
    lateinit var infoListViewModelFactory: InfoListViewModelFactory
    private lateinit var infoListViewModel: InfoListViewModel
    private lateinit var infoListRecyclerViewAdapter: InfoListRecyclerViewAdapter
    private var rowList = mutableListOf<Row>()

    private val infoListLiveData = Observer<InfoListBase> {
        it?.let {
            showTitle(it.title)
            it.rows?.let { it1 ->
                rowList.clear()
                rowList.addAll(it1)
            }
            infoListRecyclerViewAdapter.notifyDataSetChanged()
        }
    }

    override fun injectWhereNeeded(appComponent: AppComponent) {
        DaggerInfoListComponent.builder()
            .appComponent(appComponent)
            .infoListModule(InfoListModule(this))
            .build().inject(this)
    }

    override fun initializeViewModel() {
        infoListViewModel =
            ViewModelProviders.of(this, infoListViewModelFactory).get(InfoListViewModel::class.java)
    }

    override fun setObserversAndInteractor() {
        infoListViewModel.setInfoListInteractor(this)
        infoListViewModel.getInfoList().observe(this, infoListLiveData)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_info_list)
        initView()
        binding.swipeContainer.isRefreshing = true
        infoListViewModel.fetchInfoList()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initView() {
        //Set up Action bar View
        setSupportActionBar(binding.tbHome)
        actionBar = supportActionBar
        actionBar?.setDisplayShowTitleEnabled(false)
        actionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
        actionBar?.setDisplayHomeAsUpEnabled(true)
        //Setup SwipeRefreshLayout Layout
        binding.swipeContainer.setOnRefreshListener(this)
        binding.swipeContainer.setColorSchemeResources(
            R.color.colorAccent,
            android.R.color.holo_green_dark,
            android.R.color.holo_orange_dark,
            android.R.color.holo_blue_dark
        )
        setAdapter()
    }

    /**
     * Show Header title
     * */
    private fun showTitle(title: String?) {
        title?.let {
            binding.tvTitle.text = it
        }
    }

    /**
     * Setup Adapter and attached with Recycler view.
     * */
    private fun setAdapter() {
        binding.rvInfolist.layoutManager = LinearLayoutManager(this)
        infoListRecyclerViewAdapter = InfoListRecyclerViewAdapter(this, rowList, this)
        binding.rvInfolist.adapter = infoListRecyclerViewAdapter
    }

    override fun onRowItemClick(row: Row) {

    }

    override fun showNetworkErrorMsg() {
        binding.ivNointernet.visibility = View.VISIBLE
    }

    override fun showGeneralErrorMsg() {
        binding.swipeContainer.isRefreshing = false
    }

    override fun dismissProgressBar() {
        binding.swipeContainer.isRefreshing = false
    }

    /**
     * Swipe down the list and refresh
     * */
    override fun onRefresh() {
        binding.swipeContainer.isRefreshing = true
        infoListViewModel.fetchInfoList()
    }
}
