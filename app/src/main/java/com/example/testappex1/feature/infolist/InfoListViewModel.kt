package com.example.testappex1.feature.infolist

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import retrofit2.Call
import retrofit2.Response
import java.io.IOException


class InfoListViewModel(
    private val infoListFunc: InfoListFunc,
    private val infoListRestService: InfoListRestService?
) : ViewModel(), RetrofitInfoListCallback {

    private var infoListLiveData = MutableLiveData<InfoListBase>()
    private lateinit var infoListInteractor: InfoListInteractor

    /**
     * Set up Navigation as Interactuar for pass the result according.
     * */
    fun setInfoListInteractor(infoListInteractor: InfoListInteractor) {
        this.infoListInteractor = infoListInteractor
    }

    /*
    * Perform to fetch Infolist
    * */
    fun fetchInfoList() {
        infoListFunc.fetchInfoList(infoListRestService, this)
    }

    /*
    * Get Info list with are attached with Livedata
    * */
    fun getInfoList(): LiveData<InfoListBase> {
        return infoListLiveData
    }

    /**
     * Get Failure from network call and notify to UI
     * */
    override fun onFailure(call: Call<InfoListBase>, t: Throwable) {
        infoListInteractor.dismissProgressBar()
        if (t is IOException) {
            infoListInteractor.showNetworkErrorMsg()
        } else {
            infoListInteractor.showGeneralErrorMsg()
        }
    }

    /**
     * Get Success response for retrieving InfoList and set the result to Livedata
     * */
    override fun onResponse(call: Call<InfoListBase>, response: Response<InfoListBase>) {
        infoListInteractor.dismissProgressBar()
        val row = response.body()?.rows?.filter { x -> x.title != null && x.imageHref != null && x.description != null }
            ?.toMutableList()
        response.body()?.rows = row
        infoListLiveData.value = response.body()
    }

}