package com.example.testappex1.feature.infolist

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class InfoListBase(
    @JsonProperty("rows") var rows: MutableList<Row>? = mutableListOf(),
    @JsonProperty("title") var title: String?
)