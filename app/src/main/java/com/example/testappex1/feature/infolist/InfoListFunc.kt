package com.example.testappex1.feature.infolist

/**
 * Here Not require any local or other data source so that i create only one interface for
 * communication other create another class for better repository pattern.
 */
interface InfoListFunc {

    fun fetchInfoList(
        infoListRestService: InfoListRestService?,
        retrofitCallback: RetrofitInfoListCallback
    )
}