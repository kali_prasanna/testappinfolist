package com.example.testappex1.feature.infolist

class InfoListRetrofitFunImpl : InfoListFunc {

    override fun fetchInfoList(
        infoListRestService: InfoListRestService?,
        retrofitCallback: RetrofitInfoListCallback
    ) {
        infoListRestService?.getInfoList(retrofitCallback)
    }
}