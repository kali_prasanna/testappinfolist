package com.example.testappex1.feature.infolist

import android.util.Log
import com.example.testappex1.utils.RestService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InfoListRestService() : RestService() {
    private var infoListRestInterface: InfoListRestInterface =
        retrofit.create(InfoListRestInterface::class.java)

    fun getInfoList(retrofitCallback: RetrofitInfoListCallback) {
        infoListRestInterface.getInfoList()
            .enqueue(object : Callback<InfoListBase> {
                override fun onFailure(call: Call<InfoListBase>, t: Throwable) {
                    retrofitCallback.onFailure(call, t)
                }

                override fun onResponse(call: Call<InfoListBase>, response: Response<InfoListBase>) {
                    Log.e("Response", "" + response.body())
                    retrofitCallback.onResponse(call, response)
                }
            })
    }

}