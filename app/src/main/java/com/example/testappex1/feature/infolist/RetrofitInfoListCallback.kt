package com.example.testappex1.feature.infolist

import retrofit2.Call
import retrofit2.Response

interface RetrofitInfoListCallback {
    fun onFailure(call: Call<InfoListBase>, t: Throwable)

    fun onResponse(call: Call<InfoListBase>, response: Response<InfoListBase>)
}