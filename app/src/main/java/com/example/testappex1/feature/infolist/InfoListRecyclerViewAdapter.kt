package com.example.testappex1.feature.infolist

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.testappex1.R
import com.example.testappex1.databinding.ItemRowBinding

class InfoListRecyclerViewAdapter(
    private val context: Context,
    private val rowList: MutableList<Row>,
    private val rowListRecyclerViewListener: InfoListRecyclerViewListener
) : RecyclerView.Adapter<InfoListRecyclerViewAdapter.ItemrowListHolder>() {

    override fun getItemCount(): Int {
        return rowList.size
    }

    override fun onBindViewHolder(holder: ItemrowListHolder, position: Int) {
        holder.bindData(rowList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemrowListHolder {
        return ItemrowListHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.item_row,
                parent, false
            ) as ItemRowBinding
        )
    }

    inner class ItemrowListHolder(private val binding: ItemRowBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        fun bindData(row: Row) {

            if (row.imageHref.isNullOrEmpty()) {
                binding.ivItemInfo.visibility = View.GONE
            } else {
                binding.ivItemInfo.visibility = View.VISIBLE
                Glide
                    .with(context)
                    .load(row.imageHref)
                    .centerCrop()
                    .error(R.drawable.ic_no_image)
                    .into(binding.ivItemInfo)
            }

            row.title?.let {
                binding.tvItemTitle.text = it
            }
            row.description?.let {
                binding.tvItemDec.text = it
            }
            if (adapterPosition == rowList.size - 1)
                binding.ivDivider.visibility = View.GONE
            else
                binding.ivDivider.visibility = View.VISIBLE

            binding.rlItemProduct.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.rl_item_product -> {
                    rowList.get(adapterPosition).let {
                        rowListRecyclerViewListener.onRowItemClick(
                            it
                        )
                    }
                }
            }
        }
    }

    interface InfoListRecyclerViewListener {
        fun onRowItemClick(row: Row)
    }
}