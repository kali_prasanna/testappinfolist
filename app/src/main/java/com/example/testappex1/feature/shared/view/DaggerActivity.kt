package com.example.testappex1.feature.shared.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.testappex1.di.AppComponent
import com.example.testappex1.di.AppModule
import com.example.testappex1.di.DaggerAppComponent

/**
 * This class is mainly used to initialize the main component named AppComponent and sending it to the child classes
 */
abstract class DaggerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
        initializeViewModel()
        setObserversAndInteractor()
    }

    /**
     * This method initializes the AppComponent
     */
    private fun inject() {
        val appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
        injectWhereNeeded(appComponent)
    }

    /**
     * Inject the activity or fragment in the child classes
     * @param appComponent Main component of the project
     */
    protected abstract fun injectWhereNeeded(appComponent: AppComponent)

    /**
     * Initialize the ViewModels within this method.
     */
    protected abstract fun initializeViewModel()

    /**
     * Set LiveData observers and Interactors within this method.
     */
    protected abstract fun setObserversAndInteractor()

}