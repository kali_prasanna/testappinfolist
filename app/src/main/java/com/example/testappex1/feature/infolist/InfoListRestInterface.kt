package com.example.testappex1.feature.infolist

import retrofit2.Call
import retrofit2.http.GET

interface InfoListRestInterface {
    @GET("s/2iodh4vg0eortkl/facts.json")
    fun getInfoList(): Call<InfoListBase>
}