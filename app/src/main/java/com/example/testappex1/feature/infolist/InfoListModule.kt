package com.example.testappex1.feature.infolist

import dagger.Module
import dagger.Provides

@Module
class InfoListModule(private val infoListActivity: InfoListActivity) {

    @Provides
    fun getInfoRestService(): InfoListRestService? {
        return InfoListRestService()
    }

    @Provides
    fun getInfoListFunc(): InfoListFunc {
        return InfoListRetrofitFunImpl()
    }

}