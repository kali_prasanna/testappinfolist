package com.example.testappex1.feature.infolist

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import javax.inject.Inject

class InfoListViewModelFactory @Inject internal constructor(
    private val infoListFunc: InfoListFunc,
    private val infoListRestService: InfoListRestService?
) : ViewModelProvider.Factory {

    /*Creating  constructor injection and pass the desire object to ViewModel*/
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(InfoListViewModel::class.java)) {
            return InfoListViewModel(infoListFunc, infoListRestService) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}