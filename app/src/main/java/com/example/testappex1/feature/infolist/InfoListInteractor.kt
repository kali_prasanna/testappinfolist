package com.example.testappex1.feature.infolist

interface InfoListInteractor {
    fun showNetworkErrorMsg()

    fun showGeneralErrorMsg()

    fun dismissProgressBar()
}