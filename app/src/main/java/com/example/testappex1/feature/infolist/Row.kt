package com.example.testappex1.feature.infolist

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Parcelize
data class Row(
    @JsonProperty("title") var title: String?,
    @JsonProperty("description") var description: String?,
    @JsonProperty("imageHref") var imageHref: String?
) : Parcelable