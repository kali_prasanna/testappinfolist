package com.example.testappex1.feature.shared.di

import javax.inject.Scope

@Scope
annotation class FeatureScope