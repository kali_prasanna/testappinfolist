package com.example.testappex1.utils

import com.example.testappex1.constants.AppConstants
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory


/**
 * Base Retrofit service
 * */
open class RestService() {
    var retrofit: Retrofit

    init {
        val clientBuilder = OkHttpClient.Builder()
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        clientBuilder.addInterceptor(httpLoggingInterceptor)
        val objectMapper = ObjectMapper()
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        retrofit = Retrofit.Builder()
            .baseUrl(AppConstants.BaseUrls.BASE_URL)
            .client(clientBuilder.build())
            .addConverterFactory(JacksonConverterFactory.create(objectMapper))
            .build()
    }
}